# Lubimy Czytać service
## Description
Lubimy Czytać service is a lubimyczytać.pl client service for iosr course.

### Starting
```
$ gradlew bootRun
```
Server will be available at `localhost:10000`.
