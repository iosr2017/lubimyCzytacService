package pl.edu.agh.iosr.client;


import feign.Feign;
import feign.Logger;
import feign.Param;
import feign.RequestLine;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;


public interface LubimyCzytacClient {
    @RequestLine("GET /searcher/getsuggestions?phrase={query}")
    Object search(@Param("query") String query);

    @RequestLine("GET /ksiazka/{query}")
    String getMoreData(@Param("query") String query);


    static LubimyCzytacClient connect() {
        return Feign.builder()
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Logger.ErrorLogger())
                .logLevel(Logger.Level.BASIC)
                .target(LubimyCzytacClient.class, "http://lubimyczytac.pl");
    }

    static LubimyCzytacClient connectMore() {
        return Feign.builder()
                .logger(new Logger.ErrorLogger())
                .logLevel(Logger.Level.BASIC)
                .target(LubimyCzytacClient.class, "http://lubimyczytac.pl");
    }
}
