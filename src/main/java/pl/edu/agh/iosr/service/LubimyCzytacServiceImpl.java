package pl.edu.agh.iosr.service;

import com.google.gson.internal.LinkedTreeMap;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.edu.agh.iosr.client.LubimyCzytacClient;
import pl.edu.agh.iosr.model.Data;
import pl.edu.agh.iosr.model.MoreData;
import pl.edu.agh.iosr.model.Title;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class LubimyCzytacServiceImpl implements LubimyCzytacService {
    private static final Logger LOG = LoggerFactory.getLogger(LubimyCzytacService.class);

    @Override
    @HystrixCommand(fallbackMethod = "fallbackGetSuggestions")
    public Data getSuggestions(String name) {
        LubimyCzytacClient lubimyCzytacClient = LubimyCzytacClient.connect();

        LOG.info("get suggestions for: " + name);
        return new Data(lubimyCzytacClient.search(name.replace(' ','+')));
    }

    @Override
    @HystrixCommand(fallbackMethod = "fallbackGetMoreData")
    public MoreData getMoreData(String name) {
        LubimyCzytacClient lubimyCzytacClient = LubimyCzytacClient.connectMore();
        Optional<String> res = Arrays.stream(lubimyCzytacClient.getMoreData(name).split("\n")).filter(p -> p.contains("<dt>kategoria</dt>")).findFirst();
        LOG.info("get Mode Data for: " + name);
        return res.map(MoreData::new).orElse(null);
    }

    @Override
    @HystrixCommand(fallbackMethod = "fallbackGetSuggestionsBatch")
    public List<Data> getSuggestionsBatch(List<Title> input) {
        List<Data> dataList = new ArrayList<>();
        LubimyCzytacClient lubimyCzytacClient = LubimyCzytacClient.connect();
        input.forEach(t->{dataList.add(new Data(lubimyCzytacClient.search(t.getTitle().replace(' ','+'))));});
        LOG.info("get suggestions Batch: " + dataList);
        return dataList;
    }

    @HystrixCommand
    public Data fallbackGetSuggestions(String name) {
        Data result = new Data("Something went really wrong :(");
        LOG.info("fallback Get Suggestions " + name);
        return result;
    }

    @HystrixCommand
    public List<Data> fallbackGetSuggestionsBatch(List<Title> input) {
        ArrayList<Data> result = new ArrayList<>();
        result.add(new Data("Something went really wrong :("));
        LOG.info("fallback Get Suggestions Batch" + input);
        return result;
    }

    @HystrixCommand
    public MoreData fallbackGetMoreData(String name) {
        MoreData result = new MoreData("<span>Something went really wrong</span>");
        LOG.info("fallback Get More Data" + name);
        return result;
    }

}
