package pl.edu.agh.iosr.service;

import pl.edu.agh.iosr.model.Data;
import pl.edu.agh.iosr.model.MoreData;
import pl.edu.agh.iosr.model.Title;

import java.util.List;

public interface LubimyCzytacService {
    Data getSuggestions(String name);

    MoreData getMoreData(String name);

    List<Data> getSuggestionsBatch(List<Title> input);
}
