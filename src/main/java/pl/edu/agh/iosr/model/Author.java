package pl.edu.agh.iosr.model;

import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;

public class Author {
    String url;
    String fullName;

    public Author(Object authors) {
        ArrayList<Object> map = ((ArrayList)authors);
        if (map.isEmpty())return;
        setUrl((String) ((LinkedTreeMap) map.get(0)).get("url"));
        setFullName((String) ((LinkedTreeMap) map.get(0)).get("fullname"));
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }


    @Override
    public String toString() {
        return "Author{" +
                "url='" + url + '\'' +
                ", fullName='" + fullName + '\'' +
                '}';
    }
}
