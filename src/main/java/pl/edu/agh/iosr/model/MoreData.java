package pl.edu.agh.iosr.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MoreData {
//    Data data;
    String gener;

//    public Data getData() {
//        return data;
//    }
//
//    public void setData(Data data) {
//        this.data = data;
//    }


    public MoreData(String gener) {
        String patternString1 = "<span>(([A-Z|a-z|ł|ć|ę|ą|ś]*,*\\s*)*|([A-Z|a-z|ł|ć|ę|ą|ś]*\\/*\\s*)*)<\\/span>";

        Pattern pattern = Pattern.compile(patternString1);
        Matcher matcher = pattern.matcher(gener);
        matcher.find();
        this.gener = matcher.group(1);
    }

    public String getGener() {
        return gener;
    }

    public void setGener(String gener) {

        this.gener = gener;

    }

    public MoreData(Data data, String gener) {
//        this.data = data;

        this.gener = gener;
    }

}
