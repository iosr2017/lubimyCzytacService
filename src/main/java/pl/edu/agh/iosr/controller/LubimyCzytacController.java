package pl.edu.agh.iosr.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.iosr.model.Title;
import pl.edu.agh.iosr.service.LubimyCzytacService;

import java.util.List;
@RefreshScope
@RestController
public class LubimyCzytacController {

    private static final Logger LOG = LoggerFactory.getLogger(LubimyCzytacController.class);
    @Autowired
    private LubimyCzytacService lubimyCzytacService;

    @Cacheable("books")
    @RequestMapping(value = "/getBasicData/{name}", method = RequestMethod.GET)
    public Object getSuggestions(@PathVariable("name") String name) {
        LOG.info("GET getBasicData");
        return lubimyCzytacService.getSuggestions(name);
    }

    @Cacheable("books")
    @RequestMapping(value = "/getBasicData", method = RequestMethod.POST)
    public Object getSuggestionsPost(@RequestBody Title input) {
        LOG.info("POST getBasicData");
        return lubimyCzytacService.getSuggestions(input.getTitle());
    }


    @Cacheable("books")
    @RequestMapping(value = "/getBasicDataBatch", method = RequestMethod.POST)
    public Object getSuggestionsPostBatch(@RequestBody List<Title> input) {
        LOG.info("POST getBasicDataBatch");
        return lubimyCzytacService.getSuggestionsBatch(input);
    }

    @Cacheable("booksMore")
    @RequestMapping(value = "/getMoreData/{name}", method = RequestMethod.GET)
    public Object getMoreData(@PathVariable("name") String name) {
        LOG.info("GET getMoreData");
        return lubimyCzytacService.getMoreData(name);
    }

}
